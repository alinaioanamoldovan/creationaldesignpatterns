package com.company.factory;

import com.company.factory.factoryclass.ComputerFactory;
import com.company.factory.factoryclass.PcAbstractFactory;
import com.company.factory.factoryclass.ServerAbstractFactory;
import com.company.factory.superclass.Computer;

public class TestAbstractFactory {

    public static void main(String[] args){
            testFactory();
    }

    public static void testFactory(){
        Computer pc = ComputerFactory.getComputer(new PcAbstractFactory("2GB","200TB","i7"));
        Computer server = ComputerFactory.getComputer(new ServerAbstractFactory("20GB","300TB","i8"));

        System.out.println(pc);
        System.out.println(server);
    }
}
