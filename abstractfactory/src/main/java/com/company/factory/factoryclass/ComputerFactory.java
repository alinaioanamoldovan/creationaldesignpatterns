package com.company.factory.factoryclass;

import com.company.factory.superclass.Computer;

public class ComputerFactory {

    public static Computer getComputer(ComputerAbstractFactory factory){
        return factory.createComputer();
    }
}
