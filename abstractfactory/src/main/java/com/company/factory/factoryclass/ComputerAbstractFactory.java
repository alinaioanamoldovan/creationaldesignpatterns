package com.company.factory.factoryclass;

import com.company.factory.superclass.Computer;

public interface ComputerAbstractFactory {

    Computer createComputer();
}
