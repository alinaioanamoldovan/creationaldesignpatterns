package com.company.factory.factoryclass;

import com.company.factory.subclass.PC;
import com.company.factory.superclass.Computer;

public class PcAbstractFactory implements ComputerAbstractFactory {

    private String RAM;
    private String HDD;
    private String CPU;

    public PcAbstractFactory(String RAM, String HDD, String CPU) {
        this.RAM = RAM;
        this.HDD = HDD;
        this.CPU = CPU;
    }

    public Computer createComputer() {
        return new PC(RAM,HDD,CPU);
    }
}
