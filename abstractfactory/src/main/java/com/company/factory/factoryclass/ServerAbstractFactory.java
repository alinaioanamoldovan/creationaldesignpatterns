package com.company.factory.factoryclass;

import com.company.factory.subclass.Server;
import com.company.factory.superclass.Computer;

public class ServerAbstractFactory implements ComputerAbstractFactory {

    private String RAM;
    private String HDD;
    private String CPU;


    public ServerAbstractFactory(String RAM, String HDD, String CPU) {
        this.RAM = RAM;
        this.HDD = HDD;
        this.CPU = CPU;
    }

    public Computer createComputer() {
        return new Server(RAM,HDD,CPU);
    }
}
