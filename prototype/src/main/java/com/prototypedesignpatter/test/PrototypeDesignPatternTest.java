package com.prototypedesignpatter.test;

import com.prototypedesignpatter.Employee;

import java.util.List;

public class PrototypeDesignPatternTest {

    public static void main(String[] args){

        Employee emp = new Employee();
        emp.loadData();

        Employee employee2 = (Employee)emp.clone();
        Employee employee3 = (Employee)emp.clone();


        List<String> empList = employee2.getEmployee();

        empList.add("Alina45");
        empList.add("Alina56");

        List<String> empList2 = employee3.getEmployee();

        empList2.remove("Alina");

        System.out.println("Lista orginala" + emp.getEmployee());
        System.out.println("Lista unu " + empList);
        System.out.println("Lista doi " + empList2);

    }
}
