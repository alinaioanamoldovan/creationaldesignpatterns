package com.prototypedesignpatter;

import java.util.ArrayList;
import java.util.List;

public class Employee implements Cloneable {

    List<String> employee;


    public Employee() {
        this.employee = new ArrayList<String>();
    }

    public Employee(List<String> employee) {
        this.employee = employee;
    }

    public List<String> getEmployee() {
        return employee;
    }

   public void loadData(){
        employee.add("Alina");
        employee.add("Alina2");
        employee.add("Alina3");
   }

    public Object clone() {
        List<String> temp = new ArrayList<String>();
        for(String s : this.getEmployee()){
            temp.add(s);
        }
        return new Employee(temp);
    }
}
