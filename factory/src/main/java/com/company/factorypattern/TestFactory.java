package com.company.factorypattern;

import com.company.factorypattern.factory.FactoryClass;
import com.company.factorypattern.superclass.Computer;

public class TestFactory {

    public static void  main(String[] args){
        Computer pc = FactoryClass.getComputer("PC","2GB","200GB",
                "i7");
        Computer server = FactoryClass.getComputer("Server","2GB","1TB",
                "i7");

        System.out.println("PC config " + pc);
        System.out.println("Server config " + server);
    }
}
