package com.company.factorypattern.factory;

import com.company.factorypattern.subclass.PC;
import com.company.factorypattern.subclass.Server;
import com.company.factorypattern.superclass.Computer;

/**
 * This class is used to keep the instantiation of the classes independent from the client classes
 */
public class FactoryClass {

    public static Computer getComputer(String type, String ram, String hdd, String cpu) {
        Computer computer = null;
        if(type.equals("PC")){
            computer = new PC(ram,hdd,cpu);
        }else if (type.equals("Server")){
            computer = new Server(ram,hdd,cpu);
        }
        return computer;
    }
}
