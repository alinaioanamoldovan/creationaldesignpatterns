import builder.Computer;

public class TestBuilder {
    public static void main(String[] args){
        Computer c = new Computer.ComputerBuilder("100GB","20GB").setBluetoothEnabled(true).setGraphicsEnabled(true).build();


        System.out.println("With optional parameters set" + c);

        Computer c2 = new Computer.ComputerBuilder("200GB","20GB").build();

        System.out.println("Without the optional parameters " + c2);

    }
}
