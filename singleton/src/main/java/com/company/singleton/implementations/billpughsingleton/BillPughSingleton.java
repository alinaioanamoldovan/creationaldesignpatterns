package com.company.singleton.implementations.billpughsingleton;

/**
 * Prior to Java 5, java memory model had a lot of issues and above approaches
 used to fail in certain scenarios where too many threads try to get the
 instance of the Singleton class simultaneously. So Bill Pugh came up with a
 different approach to create the Singleton class using an inner static helper
 class
 When the singleton class is loaded, SingletonHelper class is
 not loaded into memory and only when someone calls the getInstance
 method, this class gets loaded and creates the Singleton class instance.

 */
public class BillPughSingleton {
    private BillPughSingleton(){}

    private static class BillPughHelperClass{
        private static final BillPughSingleton instance = new BillPughSingleton();
    }

    public static BillPughSingleton getInstance(){
        return BillPughHelperClass.instance;
    }
}
