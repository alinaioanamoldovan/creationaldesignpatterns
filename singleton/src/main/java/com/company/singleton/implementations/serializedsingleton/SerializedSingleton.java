package com.company.singleton.implementations.serializedsingleton;

import java.io.Serializable;

/**
 * The problem with above serialized singleton class is that whenever we
 deserialize it, it will create a new instance of the class. Let’s see it with a
 simple program.

 cu read resolve method deserializarea nu va crea o noua instanta, else fara aceasta methoda
 va crea doua instante la deserializare
 */
public class SerializedSingleton implements Serializable {

    private SerializedSingleton(){

    }

    private static class SerializedSingletonHelper{
        private static final SerializedSingleton INSTANCE = new SerializedSingleton();
    }

   public static SerializedSingleton getInstance(){
        return SerializedSingletonHelper.INSTANCE;
   }

   public Object readResolve(){
        return getInstance();
   }
}
