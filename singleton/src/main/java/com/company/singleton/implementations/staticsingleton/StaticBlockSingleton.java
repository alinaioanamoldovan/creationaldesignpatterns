package com.company.singleton.implementations.staticsingleton;

/**
 * This implementation of the singleton design patter that is made using a static block,static
 * block that is executed when the class is loaded. The instance ia made before the object is used so again there is
 * eager initialization which is not considered to be a good practice
 */
public class StaticBlockSingleton {

    private static final StaticBlockSingleton STATIC_BLOCK_SINGLETON;

    static {
        try{
            STATIC_BLOCK_SINGLETON = new StaticBlockSingleton();
        }catch (Exception e){
            throw new RuntimeException("The instance already exists");
        }
    }
    private StaticBlockSingleton(){}

    public static StaticBlockSingleton getInstance(){
        return STATIC_BLOCK_SINGLETON;
    }
}
