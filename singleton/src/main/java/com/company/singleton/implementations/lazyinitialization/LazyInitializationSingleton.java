package com.company.singleton.implementations.lazyinitialization;

/**
 * This singleton implementation uses lazy initialization, meaning that the instance of
 * the class  is not made before using it, when the class is loaded, the instance of the class is
 * made when the getInstance() method is called. This implementation works well when there is only
 * one thread
 */
public class LazyInitializationSingleton {

    private static LazyInitializationSingleton LAZY_INITIALIZATION_SINGLETON;
    private LazyInitializationSingleton(){

    }

    public static LazyInitializationSingleton getLazyInitializationSingleton(){
        if (LAZY_INITIALIZATION_SINGLETON == null){
            LAZY_INITIALIZATION_SINGLETON = new LazyInitializationSingleton();
        }

        return LAZY_INITIALIZATION_SINGLETON;
    }
}
