package com.company.singleton.implementations.eagersingleton;

/**
 * Type of implementation of Singleton pattern. Made with eager initialization because
 * the instance is created when the class will be loaded. Disadvantages for this implementations
 * are that it does not provides an exception handling routine and eager initialization is
 * not a good practice
 */
public class EagerSingleton {

    private static final EagerSingleton EAGER_SINGLETON  =  new EagerSingleton();
    private EagerSingleton(){ }

    public static EagerSingleton getEagerSingleton() {
        return EAGER_SINGLETON;
    }
}
