package com.company.singleton.implementations.multithread;


/**
 * This implementation works well in a multi thread environment, meaning that only one thread can
 * access the method at a time. This is a thread safe implementation but it reduces the performance
 * of the program due to the word synchronized. In order to avoid the overhead brought by the
 * synchronized word there is a method called double locked checking
 */
public class MultiThreadSingleton {

    private static MultiThreadSingleton multiThreadSingleton;

    private MultiThreadSingleton(){

    }

    public static synchronized MultiThreadSingleton getInstance(){
        if (multiThreadSingleton == null){
            multiThreadSingleton = new MultiThreadSingleton();
        }

        return multiThreadSingleton;
    }
}
