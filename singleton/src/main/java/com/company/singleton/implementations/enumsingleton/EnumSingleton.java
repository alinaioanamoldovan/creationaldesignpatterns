package com.company.singleton.implementations.enumsingleton;


/**
 * Since Java Enum values
 are globally accessible, so is the singleton. The drawback is that the enum
 type is somewhat inflexible; for example, it does not allow lazy
 initialization
 */
public enum EnumSingleton {

    INSTANCE;


    public static void doSomething(){
        //do something
    }
}
