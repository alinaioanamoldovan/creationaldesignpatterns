package com.company.singleton.implementations.doublelockedsingleton;

/**
 * This implementation is a thread safe one, and it also reduces the overhead caused by the usage of the word synchronized
 */
public class DoubleLockedChecking {

    private static  DoubleLockedChecking doubleLockedChecking;
    private DoubleLockedChecking(){}

    public static DoubleLockedChecking getDoubleLockedChecking(){
        synchronized (DoubleLockedChecking.class){
            if (doubleLockedChecking  == null){
                doubleLockedChecking = new DoubleLockedChecking();
            }
        }

        return doubleLockedChecking;
    }

}
